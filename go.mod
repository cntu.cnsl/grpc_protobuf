module gitlab.com/cntu.cnsl/grpc_protobuf

go 1.16

require (
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)

replace gitlab.com/cntu.cnsl/grpc_protobuf => ./grpc_protobuf
