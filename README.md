# gRPC_protobuf

A small go gRPC and protobuf application ( IoT learning)

# Source code directory
* README.md: This file
* sensors: Folder that stores the proto file
* server: Folder that store source code related to server
* server/sensorpb: Folder that store the compiled go code for proto
* js-client: The react js for web interactive
* envoy: Since the gRPC use HTTP/2 while most modern browsers do not support yet, we need envoy to work as proxy (HTTP/1 --> HTTP/2)

# Usage
* Run the main.go to open the gRPC server 
`go run main.go`

* Build the Envoy container
`docker build -t envoy .`

* Run the Envoy container
`docker run -p 8000:8000 -p 9901:9901 envoy`

* Run the web interface server
`cd js-client; npm start`




